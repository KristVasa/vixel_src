/*
* This library uses OpenCV to process images. The goal
* of this library is to process an image so that at the end
* we have a more precise evaluation of the text angle. The 
* most important functions are imageHoughTransform to get
* the text angle and imageRotate to rotate the image.
*/
#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <string>
#include <iostream>

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

class ImageProc {
	public :
		ImageProc();

        cv::Mat openImage(std::string imageInPath, bool color);

		cv::Mat imageGray(cv::Mat imageIn);

		cv::Mat imageRemoveNoise(cv::Mat imageIn);

		cv::Mat imageThreshold(cv::Mat imageIn);

		cv::Mat imageMorphOperation(cv::Mat imageIn, int morphOperation, 
					int dilateSize, int dilateType);

		cv::Mat imageCanny(cv::Mat imageIn);

		void imageHoughTransform(cv::Mat imageIn, double* skew);

		cv::Mat imageRotate(cv::Mat im, double thetaRad);

		~ImageProc();
};

#endif /* IMAGE_HPP_ */
