/*
* This library uses Qt to create an interface. This
* interface will make possible the interactions with
* the program. Show image, set the banlist, save 
* image etc. The most important method is 
* on_action_Open_triggered.
*/
#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <QApplication>
#include <QMainWindow>
#include <QFileDialog>
#include <QStandardPaths>
#include <QImageReader>
#include <QImageWriter>
#include <QMessageBox>
#include <QColorSpace>
#include <QScreen>
#include <QScrollArea>
#include <QScrollBar>
#include <QDebug>

#include "text.hpp"

QT_BEGIN_NAMESPACE
namespace Ui {
	class Interface;
}
QT_END_NAMESPACE

class Interface : public QMainWindow
{
    Q_OBJECT

public:
    Interface(QWidget *parent = nullptr);
    ~Interface();

private slots:
    void on_action_Open_triggered();

    void on_actionSave_As_triggered();

    void on_action_Exit_triggered();

    void on_action_Zoom_In_triggered();

    void on_action_Zoom_Out_triggered();

    void on_action_Normal_Size_triggered();

    void on_action_Fit_To_Window_triggered();

    void on_action_About_triggered();

    void on_action_Ban_List_triggered();

private:
    QImage matToQimage(cv::Mat const &mat, QImage::Format format);

    bool loadFile(const QString &fileName);

    bool saveFile(const QString &fileName);

    void scaleImage(double factor);

    void adjustScrollBar(QScrollBar *scrollBar, double factor);

    Ui::Interface *ui;
    QImage image;
    QString currentFile;

    double scaleFactor;
    std::string banListPath = "";
};

#endif /* INTERFACE_HPP */
