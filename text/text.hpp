/*
* This library uses OpenCV, Tesseract(Google) and the 
* libraries image and pixelate to detect, recognise and 
* process all text in an image. The image is processed with 
* OpenCV(+ image and pixelate) then the text detection and
* recognition is done with tesseract. And in the end the image
* is still processed with OpenCV(+ image and pixelate).
* The most important method is getWords.
*/
#ifndef TEXT_HPP_
#define TEXT_HPP_

#include <iostream>
#include <fstream>
#include <string>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/dnn/dnn.hpp>

#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

#include "pixelate.hpp"
#include "image.hpp"

class TextDR {
	public :
		TextDR();

		void boxWord(cv::Mat imageIn, int x1, int y1, int x2, int y2);

		bool cmpWord(char const* word, char const* wordToFind);

        cv::Mat getWords(std::string imageInPath, char const* language,
                    float confidence, std::string banListFilePath);

        void textWord(cv::Mat imageIn, std::string text, int x, int y);

		~TextDR();
};

#endif /* TEXT_HPP_ */
