#include "text.hpp"

/**
* Constructor does nothing.
*/
TextDR::TextDR() {
}

/**
* Boxes a word in an image.
*
* This method is used to box a word in an image.
* @param[in] imageIn The image where the word will be boxed.
* @param[in] x1 Left x.
* @param[in] y1 Top y.
* Both these parameters give the top left corner coordinates.
* @param[in] x2 Right x.
* @param[in] y2 Right y.
* Both these parameters give the bottom right corner coordinates.
*/
void TextDR::boxWord(cv::Mat imageIn, int x1, int y1, int x2, int y2) {
	cv::Point topLeft(x1, y1);
	cv::Point bottomRight(x2, y2);

	cv::rectangle(imageIn, topLeft, bottomRight, cv::Scalar(0, 255, 0));
}

/**
* Write on an image.
*
* This method is used to write on the image the word that is
* found with the function getWords.
* @param[in] imageIn The image where the word writen.
* @param[in] text Word to be writen on the image.
* @param[in] x Left x.
* @param[in] y Bottom y.
*/
void TextDR::textWord(cv::Mat imageIn, std::string text, int x, int y) {
    cv::putText(imageIn, text,
                cv::Point(x, y),
                cv::FONT_HERSHEY_COMPLEX_SMALL,
                0.9,
                cv::Scalar(0, 0, 255),
                1,
                cv:: LINE_AA);
}

/**
* Compar two words(char const*).
*
* This method is used to compare words to then have a
* banlist to pixelate the words that are on the banlist.
* @param[in] word Word to compare.
* @param[in] wordToFind Word to compare.
* @return Return true if the words are the same. False if not.
*/
bool TextDR::cmpWord(char const* word, char const* wordToFind) {
	if(strcmp(word, wordToFind) == 0) {
		return true;
	} else {
		return false;
	}
}

/**
* Detect, Recognize and process text on an image.
*
* This method is used to detect, recognize and process text on
* an image. First of all, we can give a banlist(.txt) of words to this 
* method. Then we process the image to find the angle of inclination
* of the text and rotate it of needed. Then for the detection
* and recognition of the text I use the Tesseract library.
* At the end if a word is in the banlist it will be pixelated.
* All words recognized will be writen on the image.
* @param[in] imageInPath Path to the image with text.
* @param[in] language Language to use Ex. "eng" -> english.
* @param[in] confidence On a scale of 100 the confidence needed
* 						to mark a word as recognized.
* @param[in] banListFilePath Path to the banlist a txt file.
*/
cv::Mat TextDR::getWords(std::string imageInPath, char const* language,
            float confidence, std::string banListFilePath)
{
    if(!banListFilePath.empty()) {
        cv::Mat imageIn, imageInRot, imageThreshRot, imageOut;
        Pixelate pixelateImage;
        ImageProc imageProc;

        double skew;

        std::ifstream file(banListFilePath);
        std::string line;
        std::vector<std::string> vecBanList;

        if(!file.is_open()) {
            std::cerr << "Could not open file : \""
                << banListFilePath << "\"" << std::endl;
        }

        while(std::getline(file, line, '\n')) {
            if(line != "") {
                vecBanList.push_back(line);
            }
        }

        imageIn = imageProc.openImage(imageInPath, cv::IMREAD_COLOR);

        cv::Mat imageGray 	= imageProc.imageGray(imageIn);
        cv::Mat imageBlur 	= imageProc.imageRemoveNoise(imageGray);
        cv::Mat imageThresh = imageProc.imageThreshold(imageBlur);
        cv::Mat imageOpen 	= imageProc.imageMorphOperation(imageThresh, cv::MORPH_OPEN, 3, cv::MORPH_RECT);
        cv::Mat imageClose 	= imageProc.imageMorphOperation(imageOpen, cv::MORPH_CLOSE, 3, cv::MORPH_RECT);
        cv::Mat imageCanny 	= imageProc.imageCanny(imageClose);

        imageProc.imageHoughTransform(imageCanny, &skew);

        imageThreshRot 	= imageProc.imageRotate(imageThresh, skew * CV_PI / 180);
		imageInRot 		= imageProc.imageRotate(imageIn, skew * CV_PI / 180);

        tesseract::TessBaseAPI* api = new tesseract::TessBaseAPI();

        char const* word;
        float conf;
        int x1, y1, x2, y2;

        api -> Init(NULL, language);
        api -> SetImage((uchar*) imageThreshRot.data, imageThreshRot.cols, imageThreshRot.rows,
                    imageThreshRot.channels(), imageThreshRot.step1());
        api -> Recognize(0);

        tesseract::ResultIterator* ri = api -> GetIterator();
        tesseract::PageIteratorLevel level = tesseract::RIL_WORD;

        if(ri != 0) {
            do {
                word = ri -> GetUTF8Text(level);
                conf = ri -> Confidence(level);

                if(conf > confidence) {
                    ri -> BoundingBox(level, &x1, &y1, &x2, &y2);
                    boxWord(imageInRot, x1, y1, x2, y2);
                    textWord(imageInRot, word, x1 - 2, y1 - 2);

                    for(unsigned int b = 0; b < vecBanList.size(); b++) {
                        char const* findWord = vecBanList.at(b).c_str();

                        if(cmpWord(word, findWord)) {
                            pixelateImage.pixelate(imageInRot,
                                        y1, // y top
                                        y2, // y bottom
                                        x1, // x left
                                        x2, // x right
                                        10, 10);

                            textWord(imageInRot, "XXX", x1 - 2, y1 - 2);
                        }
                    }
                }

                delete[] word;
            } while(ri -> Next(level));
        }

        file.close();

        imageOut = imageProc.imageRotate(imageInRot, (- skew) * CV_PI / 180);
        return imageOut;

    } else {
        cv::Mat imageIn, imageInRot, imageThreshRot, imageOut;
        Pixelate pixelateImage;
        ImageProc imageProc;

        double skew;

        imageIn = imageProc.openImage(imageInPath, cv::IMREAD_COLOR);

        cv::Mat imageGray 	= imageProc.imageGray(imageIn);
        cv::Mat imageBlur 	= imageProc.imageRemoveNoise(imageGray);
        cv::Mat imageThresh = imageProc.imageThreshold(imageBlur);
        cv::Mat imageOpen 	= imageProc.imageMorphOperation(imageThresh, cv::MORPH_OPEN, 0, cv::MORPH_RECT);
        cv::Mat imageClose 	= imageProc.imageMorphOperation(imageOpen, cv::MORPH_CLOSE, 0, cv::MORPH_RECT);
        cv::Mat imageCanny 	= imageProc.imageCanny(imageClose);

        imageProc.imageHoughTransform(imageCanny, &skew);

        imageThreshRot 	= imageProc.imageRotate(imageThresh, skew * CV_PI / 180);
		imageInRot 		= imageProc.imageRotate(imageIn, skew * CV_PI / 180);

        tesseract::TessBaseAPI* api = new tesseract::TessBaseAPI();

        char const* word;
        float conf;
        int x1, y1, x2, y2;

        api -> Init(NULL, language);
        api -> SetImage((uchar*) imageThreshRot.data, imageThreshRot.cols, imageThreshRot.rows,
                    imageThreshRot.channels(), imageThreshRot.step1());
        api -> Recognize(0);

        tesseract::ResultIterator* ri = api -> GetIterator();
        tesseract::PageIteratorLevel level = tesseract::RIL_WORD;

        if(ri != 0) {
            do {
                word = ri -> GetUTF8Text(level);
                conf = ri -> Confidence(level);

                if(conf > confidence) {
                    ri -> BoundingBox(level, &x1, &y1, &x2, &y2);
                    boxWord(imageInRot, x1, y1, x2, y2);
                    textWord(imageInRot, word, x1 - 2, y1 - 2);
                }

                delete[] word;
            } while(ri -> Next(level));
        }

        imageOut = imageProc.imageRotate(imageInRot, (- skew) * CV_PI / 180);
        return imageOut;
    }
}

/**
* Destructor does nothing.
*/
TextDR::~TextDR() {
}

/**
* @example example_text.cpp
* @image html positive_73.png Row image
* @image html example_text_det_rec.png (Result) Text detected and recognized
* @image html example_banlist_txt.png Banlist, composed of banned words(words to be pixelated), used on the text
*/