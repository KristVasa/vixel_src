/*
* This library uses OpenCV to pixelate images. The 
* pixelation can be on a whole image or a small portion
* of it. We can also change the size of the pixel.
* The main method is pixelate.
*/
#ifndef PIXELATE_HPP_
#define PIXELATE_HPP_

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

class Pixelate {
    public :
        Pixelate();

		void getPixel(cv::Mat imageIn, uchar* bgr, 
					uint row, uint col);

		void setPixel(cv::Mat imageIn, uchar* bgr, 
					uint row, uint col);

        void pixelate(cv::Mat imageIn,
                    uint rowBegin, uint rowEnd,
                    uint colBegin, uint colEnd,
                    uint rowPixSize, uint colPixSize);
						
		void printImage(cv::Mat imageIn, 
                    uint rowBegin, uint rowEnd,
                    uint colBegin, uint colEnd);

        ~Pixelate();
};

#endif /* PIXELATE_HPP_ */
