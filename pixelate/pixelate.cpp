#include "pixelate.hpp"

/**
* Constructor does nothing.
*/
Pixelate::Pixelate() {
}

/**
* Get the BGR of a pixel in an image.
*
* This metod uses pointers to get to the pixels of the image.
* @param[in] imageIn The image, of cv::Mat format, to scan.
* @param[out] bgr The address of an unsigned char 3 element array.
*					Where the bgr color will be stored.
* @param[in] row The row of the pixel to find.
* @param[in] col The column of the pixel to find.
*/
void Pixelate::getPixel(cv::Mat imageIn, uchar* bgr, 
			uint row, uint col) 
{
	uchar* ptr = imageIn.ptr<uchar>(row);
	ptr = ptr + (3 * col);

	bgr[0] = ptr[0];
	bgr[1] = ptr[1];
	bgr[2] = ptr[2];
}

/**
* Set the BGR of a pixel in an image.
*
* This metod uses pointers to set the pixels of the image.
* @param[in, out] imageIn The image, of cv::Mat format, to scan.
* @param[out] bgr The address of an unsigned char 3 element array.
* 					Where the bgr color to set will be read.
* @param[in] row The row of the pixel to set.
* @param[in] col The column of the pixel to set.
*/
void Pixelate::setPixel(cv::Mat imageIn, uchar* bgr, 
			uint row, uint col) 
{
	uchar* ptr = imageIn.ptr<uchar>(row);
	ptr = ptr + (3 * col);
	
	ptr[0] = bgr[0];
	ptr[1] = bgr[1];
	ptr[2] = bgr[2];
}

/**
* Pixelate a region of a photo.
*
* This metod is used to pixelate an image. The pixeltation can be
* variable and anywhere in the image. The size of the pixel also.
* The pixel and the pixelated region can be any square or rectangle.
* For this method I used the pointer method for faster treatment speed.
* The method does an average of (rowPixSize * colPixSize) then sets
* the average value on all these pixels. Then it moves to the 
* (row * rowPixSize) below till the last row. Once it reaches 
* the end it goes to the next column*colPixSize. The method takes care 
* of the borders by taking in consideration only the available pixels.
*
* Example : If we consider the following 7x8 image (or pixelation section) 
* the movement of the method goes like this: 
* 								1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9.
* For 3, 6, 7, 8, 9 the method takes into consideration only the available
* pixels. This way we can also pixelate the borders without problem.
* 
* @verbatim
	|1	|1	|1	|4	|4	|4	|7	|
	|1	|1	|1	|4	|4	|4	|7	|
	|1	|1	|1	|4	|4	|4	|7	|
	|2	|2	|2	|5	|5	|5	|8	|
	|2	|2	|2	|5	|5	|5	|8	|
	|2	|2	|2	|5	|5	|5	|8	|
	|3	|3	|3	|6	|6	|6	|9	|
	|3	|3	|3	|6	|6	|6	|9	|
 @endverbatim
*
* @param[in] imageIn The image, of cv::Mat format, to pixelate.
* @param[in] rowBegin The row where the pixelation begins.
* @param[in] rowEnd The row where the pixelation ends.
* @param[in] colBegin The column where the pixelation begins.
* @param[in] colEnd The column where the pixelation ends.
* @param[in] rowPixSize The number of pixel rows.
* @param[in] colPixSize The number of pixel columns.
*/
void Pixelate::pixelate(cv::Mat imageIn, 
			uint rowBegin, uint rowEnd,
			uint colBegin, uint colEnd,
			uint rowPixSize, uint colPixSize)
{
	uint rowPixBorder, colPixBorder, counter = 0;
	uint avgBGR[3] = {0, 0, 0};
	uchar pixGetBGR[3], pixSetBGR[3];

	for(uint row = rowBegin; row < rowEnd; row = row + rowPixSize) {
		for(uint col = colBegin; col < colEnd; col = col + colPixSize) {
			if((row + rowPixSize) > rowEnd) {
				rowPixBorder = rowEnd;
			} else {
				rowPixBorder = row + rowPixSize;
			}

			for(uint j = row; j < rowPixBorder; j++) {
				if((col + colPixSize) > colEnd) {
					colPixBorder = colEnd;
				} else {
					colPixBorder = col + colPixSize;
				}

				for(uint i = col; i < colPixBorder; i++) {
					getPixel(imageIn, pixGetBGR, j, i);

					avgBGR[0] = avgBGR[0] + pixGetBGR[0];
					avgBGR[1] = avgBGR[1] + pixGetBGR[1];
					avgBGR[2] = avgBGR[2] + pixGetBGR[2];

					counter++;
				}
			}

			pixSetBGR[0] = (uchar) (avgBGR[0] / counter);
			pixSetBGR[1] = (uchar) (avgBGR[1] / counter);
			pixSetBGR[2] = (uchar) (avgBGR[2] / counter);

			counter = 0;

			if((row + rowPixSize) > rowEnd) {
				rowPixBorder = rowEnd;
			} else {
				rowPixBorder = row + rowPixSize;
			}

			for(uint j = row; j < rowPixBorder; j++) {
				if((col + colPixSize) > colEnd) {
					colPixBorder = colEnd;
				} else {
					colPixBorder = col + colPixSize;
				}

				for(uint i = col; i < colPixBorder; i++) {
					setPixel(imageIn, pixSetBGR, j, i);
				}
			}

			avgBGR[0] = 0;
			avgBGR[1] = 0;
			avgBGR[2] = 0;
		}
	}
}

/**
* Print a region of an image in terminal (debug purposes).
*
* This metod is used to print in an image. The printing size can be
* variable and anywhere in the image. This method will print all
* 3 values of a pixel in blue -> green -> red order.
* @param[in] imageIn The image, of cv::Mat format, to print.
* @param[in] rowBegin The row where the printing begins.
* @param[in] rowEnd The row where the printing ends.
* @param[in] colBegin The column where the printing begins.
* @param[in] colEnd The column where the printing ends.
*/
void Pixelate::printImage(cv::Mat imageIn, 
			uint rowBegin, uint rowEnd,
			uint colBegin, uint colEnd) 
{
	for(uint y = rowBegin; y < rowEnd; y++) {
		for(uint x = colBegin; x < colEnd; x++) {
			printf("%d ", (uint) imageIn.at<cv::Vec3b>(y ,x)[0]);
			printf("%d ", (uint) imageIn.at<cv::Vec3b>(y ,x)[1]);
			printf("%d ", (uint) imageIn.at<cv::Vec3b>(y ,x)[2]);
			printf("\t");
		}
        printf("\n");
	}
}

/**
* Destructor does nothing.
*/
Pixelate::~Pixelate() {
}

/**
 * @example example_pixelate.cpp
 * @image html tree.jpg Row image
 * @image html example_tree_pixelated.jpg (Result) Image pixelated from Point(152, 149) to Point(237, 419) with a 25x5 pixelating filter
 */