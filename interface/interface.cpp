#include "interface.hpp"
#include "./ui_interface.h"

/**
* Constructor sets up the environement.
* 
* The constuctor will set up the environement,
* and initialise the interface.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
* @param[in] parent Parent of Interface.
*/
Interface::Interface(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Interface)
{
    ui -> setupUi(this);

    ui -> imageLabel -> setBackgroundRole(QPalette::Base);
    ui -> imageLabel -> setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    ui -> imageLabel -> setScaledContents(true);

    ui -> scrollArea -> setBackgroundRole(QPalette::Dark);
    ui -> scrollArea -> setWidget(ui -> imageLabel);
    ui -> scrollArea -> setVisible(false);
    setCentralWidget(ui -> scrollArea);

    ui -> action_Open           -> setShortcut(QKeySequence::Open);
    ui -> action_Ban_List       -> setShortcut(tr("Ctrl+B"));
    ui -> action_Exit           -> setShortcut(tr("Ctrl+Q"));
    ui -> action_Zoom_In        -> setShortcut(QKeySequence::ZoomIn);
    ui -> action_Zoom_Out       -> setShortcut(QKeySequence::ZoomOut);
    ui -> action_Normal_Size    -> setShortcut(tr("Ctrl+S"));
    ui -> action_Fit_To_Window  -> setShortcut(tr("Ctrl+F"));

    resize(QGuiApplication::primaryScreen() -> availableSize() * 3 / 5);
}

/**
* Static function that initializes a file dialog.
* 
* This method is used to initialize a file dialog.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
* @param[in] dialog Dialog.
* @param[in] acceptMode Open mode or save mode.
*/
static void initializeImageFileDialog(QFileDialog &dialog, QFileDialog::AcceptMode acceptMode) {
    static bool firstDialog = true;

    if(firstDialog) {
        firstDialog = false;
        const QStringList picturesLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
        dialog.setDirectory(picturesLocations.isEmpty() ? QDir::currentPath() : picturesLocations.last());
    }

    QStringList mimeTypeFilters;
    const QByteArrayList supportedMimeTypes = acceptMode == QFileDialog::AcceptOpen
        ? QImageReader::supportedMimeTypes() : QImageWriter::supportedMimeTypes();

    for(const QByteArray &mimeTypeName : supportedMimeTypes) {
        mimeTypeFilters.append(mimeTypeName);
    }

    mimeTypeFilters.sort();
    dialog.setMimeTypeFilters(mimeTypeFilters);
    dialog.selectMimeTypeFilter("image/jpeg");
    dialog.setAcceptMode(acceptMode);

    if(acceptMode == QFileDialog::AcceptSave) {
        dialog.setDefaultSuffix("jpg");
    }
}

/**
* Transforms an image from cv::Mat format to QImage format.
* 
* This method is transform an image of cv::Mat format to a
* QImage format to be used for show in the interface or save.
* @param[in] mat Image in cv::Mat format.
* @param[in] format QImage color format.
* @return Returns the image in QImage format.
*/
QImage Interface::matToQimage(cv::Mat const &mat, QImage::Format format) {
    return QImage(mat.data, (int) mat.cols, (int) mat.rows, (int) mat.step, format).copy();
}

/**
* Load a file.
* 
* This method load an image on the program. On top of that it
* sets some default properties of the image viewer. In this
* method we call the getWords method from text.hpp lib to detect, 
* recognise and process the images.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
* @param[in] fileName Path to image to process.
* @return True if file is loaded.
*/
bool Interface::loadFile(const QString &fileName) {
    TextDR text;

    image = matToQimage(text.getWords(fileName.toUtf8().constData(), "eng", 50.0, banListPath), QImage::Format_BGR888);

    if(image.colorSpace().isValid()) {
        image.convertToColorSpace(QColorSpace::SRgb);
    }

    ui -> imageLabel -> setPixmap(QPixmap::fromImage(image));

    scaleFactor = 1.0;

    ui -> scrollArea            -> setVisible(true);
    ui -> action_Fit_To_Window  -> setEnabled(true);

    ui -> actionSave_As         -> setEnabled(!image.isNull());
    ui -> action_Zoom_In        -> setEnabled(!ui -> action_Fit_To_Window -> isChecked());
    ui -> action_Zoom_Out       -> setEnabled(!ui -> action_Fit_To_Window -> isChecked());
    ui -> action_Fit_To_Window  -> setEnabled(!ui -> action_Fit_To_Window -> isChecked());

    if(!ui -> action_Fit_To_Window -> isChecked()) {
        ui -> imageLabel -> adjustSize();
    }

    setWindowFilePath(fileName);
    const QString message = tr("Opened \"%1\", %2x%3, Depth: %4")
            .arg(QDir::toNativeSeparators(fileName)).arg(image.width()).arg(image.height()).arg(image.depth());

    statusBar() -> showMessage(message);

    return true;
}

/**
* Open an image from the program interface
* 
* This method opens a file from the menubar of the program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_action_Open_triggered() {
    QFileDialog dialog(this, tr("Open image"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptOpen);

    while (dialog.exec() == QDialog::Accepted && !loadFile(dialog.selectedFiles().constFirst())) {}
}

/**
* Saves a file.
* 
* This method saves a file.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
* @param fileName Name of the file to save.
* @return True file is saved.
*/
bool Interface::saveFile(const QString &fileName) {
    QImageWriter writer(fileName);

    if (!writer.write(image)) {
        QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
                                 tr("Cannot write %1: %2")
                                 .arg(QDir::toNativeSeparators(fileName)), writer.errorString());
        return false;
    }
    const QString message = tr("Wrote \"%1\"").arg(QDir::toNativeSeparators(fileName));
    statusBar() -> showMessage(message);
    return true;
}

/**
* Save an image from the program interface.
* 
* This method save a file from the menubar of the program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_actionSave_As_triggered() {
    QFileDialog dialog(this, tr("Save File As"));
    initializeImageFileDialog(dialog, QFileDialog::AcceptSave);

    while (dialog.exec() == QDialog::Accepted && !saveFile(dialog.selectedFiles().constFirst())) {}
}

/**
* Set the banlist file from the program interface.
* 
* This method sets the banlist file to take into consideration
* by the getWords method. The words on the  banlist will be 
* pixelated.
*/
void Interface::on_action_Ban_List_triggered() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open the .txt banlist file"),
                                                    QDir::currentPath(), tr("Text file (*.txt)"));
    QFileInfo file(fileName);
    banListPath = file.absoluteFilePath().toUtf8().constData();
}

/**
* Exit the program.
* 
* This method exits the program.
*/
void Interface::on_action_Exit_triggered() {
    QCoreApplication::quit();
}

/**
* Adjust scroll bar.
* 
* This method adjusts the scroll bar.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::adjustScrollBar(QScrollBar *scrollBar, double factor) {
    scrollBar -> setValue(int(factor * scrollBar -> value()
                            + ((factor - 1) * scrollBar -> pageStep() / 2)));
}

/**
* Scale of the image.
* 
* This method zooms in or out of an image.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
* @param factor The factor multiplication of the image.
*/
void Interface::scaleImage(double factor) {
    scaleFactor *= factor;
    ui -> imageLabel -> resize(scaleFactor * ui -> imageLabel -> pixmap(Qt::ReturnByValue).size());

    adjustScrollBar(ui -> scrollArea -> horizontalScrollBar(), factor);
    adjustScrollBar(ui -> scrollArea -> verticalScrollBar(), factor);

    ui -> action_Zoom_In    -> setEnabled(scaleFactor < 3.0);
    ui -> action_Zoom_Out   -> setEnabled(scaleFactor > 0.333);
}

/**
* Zoom In on an image.
* 
* This method zooms in on an image from the menubar of the program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_action_Zoom_In_triggered() {
    scaleImage(1.25);
}

/**
* Zoom Out on an image.
* 
* This method zooms out on an image from the menubar of the program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_action_Zoom_Out_triggered() {
    scaleImage(0.8);
}

/**
* Display the image on normal sizes.
* 
* This method displays the image with normal sizes not multipicated by a factor.
* This is done from the menubar of the program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_action_Normal_Size_triggered() {
    ui -> imageLabel -> adjustSize();
    scaleFactor = 1.0;
}

/**
* Fits the image on window.
* 
* This method fits the image on window from the menubar of the 
* program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_action_Fit_To_Window_triggered() {
    bool fitToWindow = ui -> action_Fit_To_Window -> isChecked();
    ui -> scrollArea -> setWidgetResizable(fitToWindow);

    if (!fitToWindow) {
        ui -> imageLabel -> adjustSize();
        scaleFactor = 1.0;
    }

    ui -> actionSave_As         -> setEnabled(!image.isNull());
    ui -> action_Zoom_In        -> setEnabled(!ui -> action_Fit_To_Window -> isChecked());
    ui -> action_Zoom_Out       -> setEnabled(!ui -> action_Fit_To_Window -> isChecked());
    ui -> action_Fit_To_Window  -> setEnabled(!ui -> action_Fit_To_Window -> isChecked());
}

/**
* Display information about the program.
* 
* This method displays information about the program 
* from the menubar of the program.
* For more information :
* https://doc.qt.io/qt-5/qtwidgets-widgets-imageviewer-example.html
*/
void Interface::on_action_About_triggered() {
    QMessageBox::about(this, tr("About Vixel"),
            tr("<p><b>Vixel</b> is a text <i>detection</i> and <i>recognition</i> app. "
               "In this application the have used the following libraries     : </p>   "
               "            <p><b>OpenCV</b> -> Image processing.</p>                  "
               "            <p><b>Tesseract</b> -> Text detection and recognition.</p> "
               "            <p><b>QT</b> -> Graphical user interface.</p>              "
			   "            <p><b>Cmake</b> -> Build.</p>                              "
			   "            <p><b>Doxygen</b> -> Documentation.</p>                    "
               "<p>                                                                    "
               "The app will detect all texts on an image then it with display         "
               "the recognized text contoured by a rectangle.                          "
               "We can also add ban words. Banned words will be pixelated.</p>         "));
}

/**
* Destructor deletes the ui.
*/
Interface::~Interface() {
    delete ui;
}

/**
* @example example_interface.cpp
* @image html interface.png Row image
* @image html example_interface_about.gif About the Vixel app
* @image html example_interface_banlist.gif Adding a banlist, composed of banned words(words to be pixelated), to Vixel
* @image html example_interface_fit_to_window.gif Fitting an image to the window
* @image html example_interface_open.gif Open an image with Vixel
* @image html example_interface_save.gif Open an image with Vixel
* @image html example_interface_zoom_in.jpg Zoom in
* @image html example_interface_zoom_out.jpg Zoom out
* @image html example_interface_no_banlist.png (Result) Text detection and recognition wothout a banlist, composed of banned words(words to be pixelated)
* @image html example_interface_with_banlist.png (Result) Text detection and recognition with a banlist, composed of banned words(words to be pixelated)
*/