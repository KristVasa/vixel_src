#include "image.hpp"

/**
* Constructor does nothing.
*/
ImageProc::ImageProc() {
}

/**
* Opens an image for processing.
*
* This method is used to open an image for processing.
* @param[in] imageInPath A string to the path of the image to open.
* @param[in] color Read the image with colors or not.
* @return The read image in cv::Mat format.
*/
cv::Mat ImageProc::openImage(std::string imageInPath, bool color) {
	cv::Mat image;

    image = cv::imread(cv::samples::findFile(imageInPath), color);

	if(image.empty()) {
        printf("Error opening the file : %s\n", imageInPath.c_str());
		printf("Make sure that the path and the file's name are correct.\n");
	}

	return image;
}

/**
* Changes an image to gray nuances.
*
* This method is used to transform an image to gray nuances.
* @param[in] imageIn The image to transform.
* @return The transformed image in cv::Mat format.
*/
cv::Mat ImageProc::imageGray(cv::Mat imageIn) {
	cv::Mat imageOut;
	cv::cvtColor(imageIn, imageOut, cv::COLOR_BGR2GRAY);

	return imageOut;
}

/**
* Averages an image to remove noise.
*
* This method is used to average an image to remove noise.
* @param[in] imageIn The image to remove noise.
* @return The noise free image in cv::Mat format.
*/
cv::Mat ImageProc::imageRemoveNoise(cv::Mat imageIn) {
	cv::Mat imageOut;
	cv::medianBlur(imageIn, imageOut, 3);

	return imageOut;
}

/**
* Transform an image into a binary one.
*
* This method is used to transform an image into a binary one.
* @param[in] imageIn The image to transform to binary.
* @return The binary image in cv::Mat format.
*/
cv::Mat ImageProc::imageThreshold(cv::Mat imageIn) {
	cv::Mat imageOut;
	cv::threshold(imageIn, imageOut, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

	return imageOut;
}

/**
* Process a image based on shapes.
*
* This method is used to remove some noise, isolation of 
* individual elements and joining disparate elements in an image.
* @param[in] imageIn The image to process.
* @param[in] morphOperation The morphology operation.
* @param[in] dilateSize Size of the structoring element.
* @param[in] dilateType The shape of the structoring element.
* @return The processed image in cv::Mat format.
*/
cv::Mat ImageProc::imageMorphOperation(cv::Mat imageIn, int morphOperation, int dilateSize, int dilateType) {
	cv::Mat imageOut;

	cv::Mat kernel = cv::getStructuringElement(dilateType,
				cv::Size(2 * dilateSize + 1, 2 * dilateSize + 1),
				cv::Point(dilateSize, dilateSize));
	
	cv::morphologyEx(imageIn, imageOut, morphOperation, kernel);

	return imageOut;
}

/**
* Detect the edges.
*
* This method is used to detect the edges with the Canny
* edges detection.
* @param[in] imageIn The image to get the edges.
* @return The image in cv::Mat format with edges detected.
*/
cv::Mat ImageProc::imageCanny(cv::Mat imageIn) {
	cv::Mat imageOut;
	cv::Canny(imageIn, imageOut, 100, 200);

	return imageOut;
}

/**
* Detect the angle of the text in an image.
*
* This method used Hough method to detect the angle of
* a text in an image.
* For more information : 
* https://stackoverflow.com/questions/24046089/calculating-skew-of-text-opencv
* @param[in] imageIn The image to get the angle of it's text.
* @param[out] skew The inclination of the text in degres.
*/
void ImageProc::imageHoughTransform(cv::Mat imageIn, double* skew) {
	double max_r = sqrt(pow(0.5 * imageIn.cols, 2) + pow(0.5 * imageIn.rows, 2));
	int angleBins = 180;

	cv::Mat acc = cv::Mat::zeros(cv::Size(2 * max_r, angleBins), CV_32SC1);

	int cenx = imageIn.cols / 2;
	int ceny = imageIn.rows / 2;

	for(int x = 1; x < imageIn.cols - 1; x++) {
		for(int y = 1; y < imageIn.rows - 1; y++) {
			if(imageIn.at<uchar>(y, x) == 255) {
				for(int t = 0; t < angleBins; t++) {
					double r = (x - cenx) * cos((double) t / angleBins * CV_PI) 
								+ (y - ceny) * sin((double) t / angleBins * CV_PI);

					r += max_r;

					acc.at<int>(t, int(r))++;
				}
			}
		}
	}

	cv::Mat thresh;
	cv::normalize(acc, acc, 255, 0, cv::NORM_MINMAX);
	cv::convertScaleAbs(acc, acc);

	cv::Point maxLoc;
	cv::minMaxLoc(acc, 0, 0, 0, &maxLoc);
	
	double theta = (double) maxLoc.y / angleBins * CV_PI;
	double rho = maxLoc.x - max_r;

	if(abs(sin(theta)) < 0.000001) {
        //double m = -cos(theta) / sin(theta);

		cv::Point2d p1 = cv::Point2d(rho + imageIn.cols / 2, 0);
		cv::Point2d p2 = cv::Point2d(rho + imageIn.cols / 2, imageIn.rows);

		*skew = 90;

	} else {
		double m = -cos(theta) / sin(theta);
		double b = rho / sin(theta) + imageIn.rows / 2.0 - m * imageIn.cols / 2.0;

		cv::Point2d p1 = cv::Point2d(0, b);
		cv::Point2d p2 = cv::Point2d(imageIn.cols, imageIn.cols * m + b);

		double skewAngle;

		skewAngle = p1.x - p2.x > 0 ? (atan2(p1.y - p2.y, p1.x - p2.x) * 180.0 / CV_PI) 
					: (atan2(p2.y - p1.y, p2.x - p1.x) * 180.0 / CV_PI);

		*skew = skewAngle;
	}
}

/**
* Rotates an image.
*
* This method takes the angle of inclination of the text in
* an image and rotates the image to put the text in the
* horizontal (0° angle).
* For more information : 
* https://stackoverflow.com/questions/24046089/calculating-skew-of-text-opencv
* @param[in] imageIn The image to rotate.
* @param[out] thetaRad The angle in radiants.
* @return The rotated image.
*/
cv::Mat ImageProc::imageRotate(cv::Mat imageIn, double thetaRad) {
	cv::Mat rotated;

	double nw = abs(sin(thetaRad)) * imageIn.rows + abs(cos(thetaRad)) * imageIn.cols;
	double nh = abs(cos(thetaRad)) * imageIn.rows + abs(sin(thetaRad)) * imageIn.cols;

	cv::Mat rot_mat = cv::getRotationMatrix2D(cv::Point2d(nw * 0.5, nh * 0.5), thetaRad * 180 / CV_PI, 1);
	cv::Mat pos = cv::Mat::zeros(cv::Size(1, 3), CV_64FC1);

	pos.at<double>(0) = (nw - imageIn.cols) * 0.5;
	pos.at<double>(1) = (nh - imageIn.rows) * 0.5;

	cv::Mat res = rot_mat * pos;

	rot_mat.at<double>(0, 2) += res.at<double>(0);
	rot_mat.at<double>(1, 2) += res.at<double>(1);

	cv::warpAffine(imageIn, rotated, rot_mat, cv::Size(nw, nh), cv::INTER_LANCZOS4);

	return rotated;
}

/**
* Destructor does nothing.
*/
ImageProc::~ImageProc() {
}

/**
* @example example_image.cpp
* @image html negative_36.png Image to process
* @image html example_raw.png Row image
* @image html example_raw_plus_Gray.png Row + Gray
* @image html example_raw_plus_Gray_plus_Threshold.png Row + Gray + Threshold
* @image html example_raw_plus_Gray_plus_Threshold_plus_Opening.png Row + Gray + Threshold + Opening
* @image html example_raw_plus_Gray_plus_Threshold_plus_Opening_plus_Closing.png Row + Gray + Threshold + Opening + Closing
* @image html example_raw_plus_Gray_plus_Threshold_plus_Opening_plus_Closing_plus_Canny.png Row + Gray + Threshold + Opening + Closing + Canny Edge Detection
* @image html example_raw_plus_Rotation_plus_Angle.png (Result) Raw + Rotation + Angle of inclination
*/