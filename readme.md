# Vixel
An application that detects and recognizes test on images.
================================================

### Goal of the project

The goal of this project was to use :

[OpenCV](https://opencv.org/) -> Image processing.

[Tesseract OCR engine](https://github.com/tesseract-ocr/tesseract) -> Text detection and recognision.

[Qt](https://www.qt.io/) -> Graphical user interface.

[Cmake](https://cmake.org/) -> Build.

[Doxygen](https://www.doxygen.nl/index.html) -> Documentation.

to create an application to detect and recognizes text on images. This application can be easly modified to be able to detect text on videos also.
First of all the app. processes the images with OpenCV. Then the detection and recognition of the text is done by Tesseract(Goole maintained library). Then another image processing is done with OpenCV. Lastly the Qt is used for different interactions(Open, Save, Zoom In/Out, Add BanList etc.).

The documentation can be found [here](https://gitlab.com/KristVasa/vixel_doc/).

### Examples

Raw image :

![interface](/uploads/44f71d2d8d8927d779ec6b7afa3aa9c1/interface.png)

Resulting image without using a banlist :

![example_interface_no_banlist](/uploads/4451df0913c0eadfa0539ef1380496c9/example_interface_no_banlist.png)

Resulting image with the use of a banlist :

![example_interface_with_banlist](/uploads/e776d291f46f42b0b8491eecde174661/example_interface_with_banlist.png)

Open an image with Vixel :

![example_interface_open](/uploads/897d9df193f34205a6cac7772ac5d27e/example_interface_open.gif)

Save an image with Vixel :

![example_interface_save](/uploads/a1a3ee4e1a25212107eecadb62554e19/example_interface_save.gif)

Add a banlist on Vixel :

![example_interface_banlist](/uploads/2a128d98b13531f709652947e55d67ab/example_interface_banlist.gif))

Zoom in on Vixel :

![example_interface_zoom_in](/uploads/34b33ed41cc0027bd38e3af3daa84aa3/example_interface_zoom_in.jpg)

Zoom out on Vixel :

![example_interface_zoom_out](/uploads/07c840b6e7e441377f8433e27746034d/example_interface_zoom_out.jpg)

Fit to window on Vixel : 

![example_interface_fit_to_window](/uploads/9c0a623e92afb4cdf373ebc1fa0ba8c9/example_interface_fit_to_window.gif)

About Vixel :

![example_interface_about](/uploads/df554ea500d03a0f276bf4b86ecd497d/example_interface_about.gif)
